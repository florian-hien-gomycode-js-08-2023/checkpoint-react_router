import { Routes, Route, Link } from "react-router-dom";
import Movie from "./Components/Movie";
import MovieDetails from "./Components/MovieDetails";
import MovieList from "./Components/MovieList";

import "./App.css";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" index element={<Movie />}></Route>
        <Route path=":id" element={<MovieDetails />}></Route>
      </Routes>
    </div>
  );
}

export default App;
