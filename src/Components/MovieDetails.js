import { useParams } from "react-router-dom";
import MovieList from "./MovieList";
import React, { useState, useEffect}  from "react";


function MovieDetails() {
    const [film, setFilm] = useState({});
  const { id } = useParams();

  useEffect(() => {
    const movie = MovieList.find(el => el.id === id)
    setFilm(movie)
    console.log(film.trailer);
  }, [id]);
  
  return (
    <div>
      <p>MovieDetails</p>
      <h1>...</h1>
      <img src={film.trailer} alt="" />
      <p>{film.id}</p>
      <p>{film.title}</p>
      <p>{film.rating}</p>
    </div>
  );
}

export default MovieDetails;
