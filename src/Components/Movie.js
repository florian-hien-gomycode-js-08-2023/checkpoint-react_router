import MovieCard from "./MovieCard";
import Filter from "./Filter";
import MovieList from "./MovieList";
import React, { useState, useEffect, useRef } from "react";
import {  Outlet } from "react-router-dom";

 
function Movie() {
  const [list, setList] = useState([]);

  const [titleFilter, setTitleFilter] = useState("");
  const [rateFilter, setRateFilter] = useState(null);

  const [films, setFilms] = useState([...MovieList]);

  function updateTitle(title) {
    setTitleFilter(title);
  }
  function updateRate(rate) {
    setRateFilter(Number(rate));
  }

  const addArticle = (newArticle) => {
    MovieList.push(newArticle)
    setFilms([...films, newArticle]);
  };

  useEffect(() => {
    setList(films);
  }, [films]);

  useEffect(() => {
    const filmsFiltrés = films.filter(
      (film) =>
        film.title.toLowerCase().includes(titleFilter.toLowerCase().trim()) &&
        (rateFilter ? Number(film.rating) === rateFilter : true)
    );
    setList(filmsFiltrés);
  }, [titleFilter, rateFilter]);

  const h = window.innerHeight;
  const mH = { height: h - h / 14 };
 
  return (
    <>
      <div className="left">
        <Filter
          updateTitle={updateTitle}
          updateRate={updateRate}
          addArticle={addArticle}
        ></Filter>
      </div>

      <div className="right" style={mH}>
        <div>
          <h2>Movies</h2>
        </div>
        <div className="movies">
          {list.map((el) => (
            <MovieCard
              id={el.id}
              posterURL={el.posterURL}
              title={el.title}
              description={el.description}
              rating={el.rating}
            ></MovieCard>
          ))}
        </div>
      </div> 
      {/* <Outlet></Outlet> */}
    </>
  );
}

export default Movie;
