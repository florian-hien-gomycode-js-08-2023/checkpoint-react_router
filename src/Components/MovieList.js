const MovieList = [
  {
    id:'film-0',
    posterURL:
    "https://m.media-amazon.com/images/M/MV5BMTUxOGE3OTUtM2I2My00YzE3LTg2NDktZTI3NjE4NDdjMGFiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg",
    title: "Darkest Mind",
    description:
      "In the near future, teenagers have been decimated by an unknown virus. The survivors.",
    rating: 10,
    trailer:"https://image.tmdb.org/t/p/w1280/cfBD5UlPL5Ibig511XiqLUVwlsH.jpg"
  },
  {
    id:'film-1',
    posterURL:
    "https://m.media-amazon.com/images/M/MV5BYmY3NGJlYTItYmQ4OS00ZTEwLWIzODItMjMzNWU2MDE0NjZhXkEyXkFqcGdeQXVyMzQzMDA3MTI@._V1_.jpg",
    title: "Doctor Sleep",
    description:
      "Irrevocably marked by the trauma he endured as a child at the Overlook, Dan Torrance.",
    rating: 10,
    trailer:"https://wallpapers.com/images/hd/official-doctor-sleep-movie-poster-w71asotv1en11ufv.jpg"
  },
  {
    id:'film-2',
    posterURL:
    "https://i.pinimg.com/originals/80/bf/38/80bf38f8e7fa9d98ccd8e180b48c2340.jpg",
    title: "Annabelle",
    description:
      "Annabelle is an American horror film directed by John R. Leonetti and released in 20.",
    rating: 9,
    trailer:"https://assets-in.bmscdn.com/discovery-catalog/events/et00046520-wkjefrmfcu-landscape.jpg"
  },
];

export default MovieList;
