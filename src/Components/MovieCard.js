import React, { useState, useEffect } from "react";
import { Link,Outlet } from "react-router-dom";

import pencil from "./pencil.svg";

function MovieCard({ id, posterURL, title, description, rating }) {
  return (
    <>
      <div className="" id={id}>
      <Link to={id}>
      <div className="wrapper">
        <div className="element-grow">
          <img
            src={posterURL}
            alt=""
            style={{ width: 230, height: 370, borderRadius: 25 }}
          />
        </div>
        <div className="details">
          <p>
            <span>Title: </span>
            {title}
          </p>
          <p>
            <span>Description: </span>
            {description}
          </p>
          <p>
            <span>Rate: </span>
            {rating}/10
          </p>
        </div>
      </div>
    </Link>

      </div>
    </>
  );
}

export default MovieCard;
